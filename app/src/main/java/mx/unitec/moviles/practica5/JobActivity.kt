package mx.unitec.moviles.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.unitec.moviles.practica5.service.MylobintentService

class JobActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job2)

        val intent = Intent(this, MylobintentService::class.java)
        intent.putExtra("max", 100)
        MylobintentService.enqueueWork(this, intent)

    }
}